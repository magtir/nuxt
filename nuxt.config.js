module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title: 'nuxt',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Nuxt.js project'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {
                rel: "stylesheet",
                href: "/fonts/fonts.css"
            },
            {
                rel: "stylesheet",
                href: "/common/owl.carousel/owl.carousel.min.css"
            },
            {
                rel: "stylesheet",
                href: "/common/owl.carousel/owl.theme.default.min.css"
            },
            {
                rel: "stylesheet",
                href: "/common/owl.carousel/owl.theme.default.min.css"
            }
        ],
        script: [
            {
                src: "/common/jquery.min.js",
                type: "text/javascript"
            },
            {
                src: "/common/owl.carousel/owl.carousel.min.js",
                type: "text/javascript"
            },
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#3B8070'},
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        },
        babel: {
            presets({isServer}) {
                return [
                    [
                        require.resolve('@nuxt/babel-preset-app'),
                        // require.resolve('@nuxt/babel-preset-app-edge'), // For nuxt-edge users
                        {
                            targets: isServer ? {node: '10'} : {ie: '11'},
                            corejs: {version: 3}
                        }
                    ]
                ]
            }
        }
    },
    css: [
        { src: '~assets/g-styles/main.less', lang: 'less' },
    ],
    router: {
        scrollBehavior: async (to, from, savedPosition) => {
            if (savedPosition) {
                return savedPosition
            }

            const findEl = async (hash, x) => {
                return document.querySelector(hash) ||
                    new Promise((resolve, reject) => {
                        if (x > 50) {
                            return resolve()
                        }
                        setTimeout(() => { resolve(findEl(hash, ++x || 1)) }, 100)
                    })
            };

            if (to.hash) {
                let el = await findEl(to.hash.replace('/', ''));
                if ('scrollBehavior' in document.documentElement.style) {
                    return window.scrollTo({ top: el.offsetTop, behavior: 'smooth' })
                } else {
                    return window.scrollTo(0, el.offsetTop)
                }
            }

            return { x: 0, y: 0 }
        }
    }
};

