export default class PricingCarousel {
    constructor() {
        let mouseDown = false;

        $('.js-pricing-carousel').owlCarousel({
            items: 3,
            dotsEach: true,
            slideBy: 1,
            dots: true,
            nav: true,
            loop: false,
            margin: 10,
            mouseDrag: true,
            touchDrag: true,
            responsive: {
                0: {
                    autoWidth: true,
                    margin: 5,
                    dots: false,
                    nav: false
                },
                768: {
                    autoWidth: true,
                    margin: 12,
                    dots: false,
                    nav: false
                },
                1024: {
                    autoWidth: true,
                    margin: 20
                },
                1200: {
                    margin: 30
                }

            }
        });

        $(document)
            .on('mousedown', '.owl-dot', () => {
                mouseDown = true;
            })
            .on('mouseup', () => {
                mouseDown = false;
            })
            .on('mousemove', '.owl-dot', function() {
                if (mouseDown) {
                    let $this = $(this);
                    $this.trigger('click');
                }
            });
    }

}
