export default class TabsFixedCarousel {

    constructor() {
        let arrPath = window.location.pathname.split('/');
        let num = arrPath[arrPath.length - 1];

        // $('.js-fixed-tabs').trigger('destroy.owl.carousel');
        $('.js-fixed-tabs').owlCarousel({
            items: 1,
            dots: false,
            nav: false,
            loop: false,
            margin: 0,
            autoWidth: true,
            mouseDrag: true,
            touchDrag: true,
            startPosition: +num - 1,
            center:true,
            responsive: {
                0: {
                    nav: false
                },
                768: {
                    nav: false
                },
                1024: {
                    autoWidth: false,
                    items: 3,
                    nav: true
                },
                1200: {
                    autoWidth: false,
                    items: 4,
                    nav: true
                }

            }
        });
    }

}
