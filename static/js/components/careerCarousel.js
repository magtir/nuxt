export default class CareerCarousel {

    constructor() {
        $('.js-career-carousel').owlCarousel({
            items: 1,
            dots: false,
            nav: false,
            loop:true,
            margin: 10,
            autoWidth: true,
            mouseDrag: true,
            touchDrag: true,
            responsive: {
                0: {
                    margin: 10
                },
                768: {
                    margin: 12,
                },
                1024: {
                    margin: 20,
                },
                1200: {
                    margin: 30,
                }

            },
            onDrag: () => {
                this.onDrag();
            },
            onTranslated: () => {
                this.onTranslated();
            }
        });
    }

    onDrag() {
    }

    onTranslated() {
    }
}
